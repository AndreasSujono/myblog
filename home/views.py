from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.conf import settings
from django.core.paginator import Paginator

from django.contrib.auth.models import User
from userAdmin.models import Post,Comment

import requests
from datetime import datetime


# Create your views here.
def home_view(request,*args,**kwargs):

	login = False
	data = {}

	posts_list = Post.objects.all()
	paginator = Paginator(posts_list, 2)

	page = request.GET.get('page')
	posts = paginator.get_page(page)

	if request.user.is_authenticated :
		login = True
		data = {
			'username' : request.user,

		}


	payload = {
		'content': 'testing',
		'login' : login,
		'data' : data,
		'posts' : posts,
	}

	return render(request,'home.html',payload)

def signUp_view(request,*args,**kwargs):
	return render(request, 'signUp.html', {'error':None})


def signup_handle(request):
	email = request.POST['email']
	username = request.POST['username']
	password = request.POST['password']
	confirmPassword = request.POST['confirmPassword']
	recaptcha = request.POST['g-recaptcha-response']

	print(recaptcha)
	'''
	data = {
		'secret' : settings.GOOGLE_RECAPTCHA_SECRET_KEY,
		'response' : recaptcha 
	}
	r = requests.post(url = 'https://www.google.com/recaptcha/api/siteverify', data= data)

	print(r)
	'''



	#handle form validation
	if email == '' or username == '' or password == '' or confirmPassword == '':
		messages.error(request, 'form cannot be empty')
		return redirect('/signUp')

	if password != confirmPassword:
		messages.error(request, 'passwords are not match')
		return redirect('/signUp')

	try :
		User.objects.get(username=username)
		messages.error(request, 'username is already exist')
		return redirect('/signUp')

	except:
		user = User.objects.create_user(
		    username = username,
		    password = password,
		    email = email
		)

		user.save()

		login(request,user)
		messages.success(request, 'sign up success, you are logged in now')

		return redirect('/')



def login_handle(request):

	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(username=username,password=password)
		print(user)

		if user is not None:
			login(request, user)

			messages.success(request, 'you are logged in')
			return redirect('/')

	messages.error(request, 'wrong username or password')

	page = request.GET.get('page')

	return redirect(f'/?page={page}')


def logout_view(request):
	logout(request)

	return redirect('/')

def individualPost_view(request,id):
	login = False
	post = Post.objects.get(id= id)

	comments = reversed(Comment.objects.filter(post = post) )
	print(comments)

	if request.user.is_authenticated :
		login = True

	return render(request, 'individualPost.html', {'post':post,'login':login,'user':request.user,'comments':comments})

def addComment_handler(request,id):
	post = Post.objects.get(id=id)
	user = User.objects.get(username=request.user)

	detail = request.POST['comment']

	now = datetime.now()
	date = now.strftime("%Y-%m-%d")

	comment = Comment(post=post,user=user,detail=detail,date=date)
	comment.save()

	return redirect(f'/post/{id}')





