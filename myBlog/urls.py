
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from home.views import home_view
from home.views import signUp_view
from home.views import login_handle
from home.views import signup_handle
from home.views import logout_view
from home.views import individualPost_view

from userAdmin.views import admin_view
from userAdmin.views import userProfile_view
from userAdmin.views import userProfile_update
from userAdmin.views import addNewContent_view
from userAdmin.views import addNewContent_handler
from userAdmin.views import allContent_view
from userAdmin.views import editDelete_view
from userAdmin.views import editDelete_handler
from home.views import addComment_handler




urlpatterns = [
	path('',home_view,name='home'),
	path('signUp/',signUp_view ,name='sign up'),
    path('login/',login_handle ,name='login'),
    path('signup_handle/',signup_handle ,name='signup_handle'),
    path('post/<int:id>/',individualPost_view ,name='individualPost_view'),

    path('userAdmin/',admin_view,name= 'user_admin'),
    path('userAdmin/userProfile/',userProfile_view,name= 'userProfile_view'),
    path('userAdmin/userProfile/update/',userProfile_update,name= 'userProfile_update'),
    path('userAdmin/addContent/',addNewContent_view,name= 'addNewContent_view'),
    path('userAdmin/addContent/add',addNewContent_handler,name= 'addNewContent_handler'),
    path('userAdmin/allContent/',allContent_view,name= 'allContent_view'),
    path('userAdmin/editDelete/<int:id>/',editDelete_view,name= 'editDelete_view'),
    path('userAdmin/editDelete/handle/<int:id>/',editDelete_handler,name= 'editDelete_handler'),
    path('userAdmin/addComment/<int:id>/',addComment_handler,name= 'addComment_handler'),

    path('logout/',logout_view,name='logout'),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

