from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    pub_date = models.DateField()
    content = models.TextField()
    image = models.ImageField(blank=True,upload_to='postImages')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')

class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    profile_pict = models.ImageField(blank=True,upload_to='profile_pictures')

class Comment(models.Model):

    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='post')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    detail = models.TextField(blank=True)
    date = models.DateField( blank = True)