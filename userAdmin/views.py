from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.paginator import Paginator
from datetime import datetime
from django.conf import settings

import os

from django.core.files.storage import FileSystemStorage

from .models import Post,Profile


# Create your views here.
@login_required
def admin_view(request):
	return render(request,'admin.html',{})

@login_required
def userProfile_view(request):

	user = User.objects.get(username = request.user)

	return render(request, 'userProfile.html', {'user':user})

@login_required
def userProfile_update(request):

	email = request.POST['email']
	username = request.POST['username']
	

	user = User.objects.get(username = request.user)

	try:
		image = request.FILES['image']
		try:
			profile = Profile.objects.get(user = request.user)
			profile.profile_pict = image
			profile.save()

		except:
			profile = Profile(user = request.user, profile_pict = image)
			profile.save()

		messages.success(request,'profile picture updated')	
	except:
		pass


	if (username == str(request.user)):

		if (email != User.objects.get(username= request.user).email):
			user.email = email
			user.save()

			messages.success(request,'email is updated')

	else:
		try:
			#if updated username exists
			User.objects.get(username = username)

			messages.error(request, 'username is already exist')
			


		except:
			user.username = username
			user.email = email
			user.save()

			messages.success(request,'email and username are updated')

	return redirect('/userAdmin/userProfile')

@login_required
def addNewContent_view(request):
	return render(request,'addNewContent.html', {})

@login_required
def addNewContent_handler(request):
	title = request.POST['title']
	content = request.POST['content']
	image = request.FILES['image']

	username = request.user
	now = datetime.now()
	pub_date = now.strftime("%Y-%m-%d")

	#validation
	if (title == '' or content == ''):
		messages.error(request, 'Field cannot be empty')
		return redirect('/userAdmin/addContent')

	post = Post(title = title, content=content, image=image, author=username, pub_date = pub_date)
	post.save()

	messages.success(request, 'new content added')

	return redirect('/userAdmin/addContent')
	


@login_required
def allContent_view(request):

	posts_list = Post.objects.all()
	paginator = Paginator(posts_list, 5)

	page = request.GET.get('page')
	posts = paginator.get_page(page)

	user = request.user

	return render(request,'allContent.html', {'posts':posts,'user':user})

@login_required
def editDelete_view(request,id):
	post = Post.objects.get(id=id)

	if request.user == post.author:
		return render(request,'editDelete.html', {'post':post} )

	return False

@login_required
def editDelete_handler(request,id):

	post = Post.objects.get(id=id)

	if request.POST['submit'] == 'edit':
		title = request.POST['title']
		content = request.POST['content']
		

		username = request.user
		now = datetime.now()
		pub_date = now.strftime("%Y-%m-%d")

		#validation
		if (title == '' or content == ''):
			messages.error(request, 'Field cannot be empty')
			return redirect(f'/userAdmin/editDelete/{id}')

		post.title = title
		post.content = content

		try:
			image = request.FILES['image']
			post.image = image
		except:
			pass

		post.pub_date = pub_date

		post.save()

		messages.success(request,'content has been edited')
		return redirect('/userAdmin/allContent')


	elif request.POST['submit'] == 'delete':
		post.delete()

		messages.success(request,'content has been deleted')

		return redirect('/userAdmin/allContent')







